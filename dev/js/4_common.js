jQuery(document).ready(function($) {
    $(window).load(function() {
        //		$('body').css('overflow-y','auto');
    });
    /*
     * Mobile nav
     */
    $(".mob_menu_link").on("click", function() {
        $(".mob_menu_link").toggleClass("active");
        $(".header__center").toggleClass("active");
    });
    /*
     * Tabs
     */
    var tab;
    var tabContent;

    window.onload = function() {
        tabContent = document.getElementsByClassName("tabContent");
        tab = document.getElementsByClassName("tab");
        hideTabsContent(1);
    };

    function hideTabsContent(a) {
        for (var i = a; i < tabContent.length; i++) {
            tabContent[i].classList.remove("show");
            tabContent[i].classList.add("hide");
            tab[i].classList.remove("active");
            $(tabContent).each(function() {
                if ($(this).hasClass("hide")) {
                    $(this)
                        .find(".slider-project")
                        .slick("unslick");
                }
            });
        }
    }

    document.getElementById("tabs").onclick = function(event) {
        var target = event.target;
        if (target.className == "tab") {
            for (var i = 0; i < tab.length; i++) {
                if (target == tab[i]) {
                    showTabsContent(i);
                    break;
                }
            }
        }
    };

    function showTabsContent(b) {
        if (tabContent[b].classList.contains("hide")) {
            hideTabsContent(0);
            tab[b].classList.add("active");
            tabContent[b].classList.remove("hide");
            tabContent[b].classList.add("show");
            $(".slider-project").slick({
                prevArrow: '<button class="arrow-prev "><img src="img/prev.png"></button>',
                nextArrow: '<button class="arrow-next "><img src="img/next.png"></button>'
            });
        }
    }

    /*
     * Slider
     */
    $(".slider-project").slick({
        prevArrow: '<button class="arrow-prev "><img src="img/prev.png"></button>',
        nextArrow: '<button class="arrow-next "><img src="img/next.png"></button>'
    });
    $(".gallery-slider").slick({
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        prevArrow: '<button class="arrow-prev "><img src="img/prev.png"></button>',
        nextArrow: '<button class="arrow-next "><img src="img/next.png"></button>'
    });
    /*
     * PopUp
     */
    $(".pop-up .close").click(function(e) {
        e.preventDefault();
        $(".pop-up").hide();
        setInterval(function() {
            $(".pop-up").css("display", "flex");
        }, 50000);
    });
    setTimeout(function() {
        $(".pop-up").css("display", "flex");
        setTimeout(function() {
            $(".pop-up").hide();
            setInterval(function() {
                $(".pop-up").css("display", "flex");
            }, 50000);
        }, 10000);
    }, 10000);
    setInterval(function() {
        $(".pop-up").css("display", "flex");
        setTimeout(function() {
            $(".pop-up").hide();
        }, 10000);
    }, 60000);

    $("form").submit(function(e) {
        e.preventDefault();
        let dataForm;
        if (
            $(this)
                .find("input[name=forms_name]")
                .val() == "Узнать подробнее"
        ) {
            dataForm = {
                phone: $(this)
                    .find("input[name=phone]")
                    .val(),
                name: $(this)
                    .find("input[name=name]")
                    .val(),
                form_name: $(this)
                    .find("input[name=forms_name]")
                    .val()
            };
            console.log("data ", dataForm);
            console.log("target ", e.target);
        }
        if (
            $(this)
                .find("input[name=form_name]")
                .val() == "apartment"
        ) {
            dataForm = {
                phone: $(this)
                    .find("input[name=phone]")
                    .val(),
                apartment: $(this)
                    .find("input[name=apartment]")
                    .val()
            };
            console.log("data ", dataForm);
            console.log("target ", e.target);
        }
        if (
            $(this)
                .find("input[name=forms_name]")
                .val() == "Дизайн квартир"
        ) {
            dataForm = {
                phone: $(this)
                    .find("input[name=phone]")
                    .val(),
                name: $(this)
                    .find("input[name=name]")
                    .val(),
                email: $(this)
                    .find("input[name=email]")
                    .val(),
                form_name: $(this)
                    .find("input[name=forms_name]")
                    .val()
            };
            console.log("data ", dataForm);
            console.log("target ", e.target);
        }
        if (
            $(this)
                .find("input[name=forms_name]")
                .val() == "Узнать подробнее 2"
        ) {
            dataForm = {
                phone: $(this)
                    .find("input[name=phone]")
                    .val(),
                name: $(this)
                    .find("input[name=name]")
                    .val(),
                form_name: $(this)
                    .find("input[name=forms_name]")
                    .val()
            };
            console.log("data ", dataForm);
            console.log("target ", e.target);
        }
        if (
            $(this)
                .find("input[name=forms_name]")
                .val() == "pop_up"
        ) {
            dataForm = {
                phone: $(this)
                    .find("input[name=phone]")
                    .val(),
                name: $(this)
                    .find("input[name=name]")
                    .val(),
                form_name: $(this)
                    .find("input[name=forms_name]")
                    .val()
            };
            console.log("data ", dataForm);
            console.log("target ", e.target);
        }

        $.ajax({
            type: "post",
            url: "ajax.php",
            data: dataForm,
            success: function(res) {
                console.log(res);
                Swal.fire("Ваша Заявка была отправлена", "Мы скоро с Вами свяжемся", "success");
            }
        });
    });
});
